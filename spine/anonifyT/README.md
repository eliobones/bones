![](https://elioway.gitlab.io/ribs/boilerT/elio-boiler-t-logo.png)

> Will there be paradise or will there be loss?, **the elioWay**

# boilerT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [boilerT Documentation](https://elioway.gitlab.io/ribs/boilerT/)

## Prerequisites

- [boilerT Prerequisites](https://elioway.gitlab.io/ribs/boilerT/installing.html)

## Installing

- [Installing boilerT](https://elioway.gitlab.io/ribs/boilerT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [boilerT Quickstart](https://elioway.gitlab.io/ribs/boilerT/quickstart.html)

# Credits

- [boilerT Credits](https://elioway.gitlab.io/ribs/boilerT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/boilerT/apple-touch-icon.png)
