![](https://elioway.gitlab.io/ribs/schemaT/elio-schema-t-logo.png)

> SchemaT, **the elioWay**

# schemaT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [schemaT Documentation](https://elioway.gitlab.io/ribs/schemaT/)

## Prerequisites

- [schemaT Prerequisites](https://elioway.gitlab.io/ribs/schemaT/installing.html)

## Installing

- [Installing schemaT](https://elioway.gitlab.io/ribs/schemaT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [schemaT Quickstart](https://elioway.gitlab.io/ribs/schemaT/quickstart.html)

# Credits

- [schemaT Credits](https://elioway.gitlab.io/ribs/schemaT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/schemaT/apple-touch-icon.png)
