![](https://elioway.gitlab.io/ribs/updateT/elio-update-t-logo.png)

> UpdateT, **the elioWay**

# updateT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [updateT Documentation](https://elioway.gitlab.io/ribs/updateT/)

## Prerequisites

- [updateT Prerequisites](https://elioway.gitlab.io/ribs/updateT/installing.html)

## Installing

- [Installing updateT](https://elioway.gitlab.io/ribs/updateT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [updateT Quickstart](https://elioway.gitlab.io/ribs/updateT/quickstart.html)

# Credits

- [updateT Credits](https://elioway.gitlab.io/ribs/updateT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/updateT/apple-touch-icon.png)
