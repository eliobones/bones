![](https://elioway.gitlab.io/ribs/enlistT/elio-enlist-t-logo.png)

> EnlistT, **the elioWay**

# enlistT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [enlistT Documentation](https://elioway.gitlab.io/ribs/enlistT/)

## Prerequisites

- [enlistT Prerequisites](https://elioway.gitlab.io/ribs/enlistT/installing.html)

## Installing

- [Installing enlistT](https://elioway.gitlab.io/ribs/enlistT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [enlistT Quickstart](https://elioway.gitlab.io/ribs/enlistT/quickstart.html)

# Credits

- [enlistT Credits](https://elioway.gitlab.io/ribs/enlistT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/enlistT/apple-touch-icon.png)
