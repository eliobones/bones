![](https://elioway.gitlab.io/engageT/permitT/elio-permit-t-logo.png)

> PermitT, **the elioWay**

# permitT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [permitT Documentation](https://elioway.gitlab.io/engageT/permitT/)

## Prerequisites

- [permitT Prerequisites](https://elioway.gitlab.io/engageT/permitT/installing.html)

## Installing

- [Installing permitT](https://elioway.gitlab.io/engageT/permitT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [engageT Quickstart](https://elioway.gitlab.io/engageT/quickstart.html)
- [permitT Quickstart](https://elioway.gitlab.io/engageT/permitT/quickstart.html)

# Credits

- [permitT Credits](https://elioway.gitlab.io/engageT/permitT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/engageT/permitT/apple-touch-icon.png)
