![](https://elioway.gitlab.io/ribs/paradiseLosterT/elio-paradiseLoster-t-logo.png)

> Will there be paradise or will there be loss?, **the elioWay**

# paradiseLosterT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [paradiseLosterT Documentation](https://elioway.gitlab.io/ribs/paradiseLosterT/)

## Prerequisites

- [paradiseLosterT Prerequisites](https://elioway.gitlab.io/ribs/paradiseLosterT/installing.html)

## Installing

- [Installing paradiseLosterT](https://elioway.gitlab.io/ribs/paradiseLosterT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [paradiseLosterT Quickstart](https://elioway.gitlab.io/ribs/paradiseLosterT/quickstart.html)

# Credits

- [paradiseLosterT Credits](https://elioway.gitlab.io/ribs/paradiseLosterT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/paradiseLosterT/apple-touch-icon.png)
