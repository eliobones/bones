![](https://elioway.gitlab.io/ribs/undoT/elio-undo-t-logo.png)

> UndoT, **the elioWay**

# undoT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [undoT Documentation](https://elioway.gitlab.io/ribs/undoT/)

## Prerequisites

- [undoT Prerequisites](https://elioway.gitlab.io/ribs/undoT/installing.html)

## Installing

- [Installing undoT](https://elioway.gitlab.io/ribs/undoT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [undoT Quickstart](https://elioway.gitlab.io/ribs/undoT/quickstart.html)

# Credits

- [undoT Credits](https://elioway.gitlab.io/ribs/undoT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/undoT/apple-touch-icon.png)
